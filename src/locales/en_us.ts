export const EN_US: string = `{
	"general.login": "Login",
	"general.register": "Register",
	"general.login.success": "Successful login",
	"general.error": "Error",
	"general.login.error": "Unable to login, check your credentials",
	"general.login.welcome": "Welcome "
}`;
