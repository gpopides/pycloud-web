import {EN_US} from '../locales/en_us';

const convertMessage = (messageId: string): string => {
  const localeText = JSON.parse(EN_US)[messageId];

  return localeText !== undefined ? localeText : 'N/A';
};

export {convertMessage};
