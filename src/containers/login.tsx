import React, {useState} from 'react';
import {Grid, Button, Loader, Input, Row, Col} from 'rsuite';
import LocaleMessage from '../components/localeMessage';
import {connect} from 'react-redux';
import {LoginComponentState} from '../interfaces/connectInterfaces';
import {RootState} from '../store/types/auth';
import {actions} from '../store/actions/auth';
import {actions as appActions} from '../store/actions/app';

const {loginRequest} = actions;
const {redirect} = appActions;

const Login: React.FC = (props: any) => {
  const [username, updateUsername] = useState<string | undefined>(undefined);
  const [password, updatePassword] = useState<string | undefined>(undefined);

  const handleSubmit = (): any => {
    props.loginRequest(username, password);
  };

  const handleInputChange = (inputField: string) => (value: string): any => {
    inputField === 'username' ? updateUsername(value) : updatePassword(value);
  };

	const redirectRegister = () => {
		props.redirect("/register");
	}

  return (
    <Grid align={'middle'} style={{marginTop: '20%'}}>
      {props.loading && <Loader content={'Loading modafaka'} />}
      <Row>
        <Col xs={24}>
          <Input
            value={username}
            onChange={handleInputChange('username')}
            style={{marginTop: '10px', width: '300px'}}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={24}>
          <Input
            type={'password'}
            value={password}
            onChange={handleInputChange('password')}
            style={{marginTop: '10px', width: '300px'}}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={21} lgPush={3}>
          <Button onClick={handleSubmit} style={{marginTop: '10px'}}>
            <LocaleMessage messageId={'general.login'} />
          </Button>
          <Button
            onClick={redirectRegister}
            color={'yellow'}
            style={{margin: '10px 0 0 10px'}}>
            <LocaleMessage messageId={'general.register'} />
          </Button>
        </Col>
      </Row>
    </Grid>
  );
};

const mapActionsToProps = {
  loginRequest,
	redirect
};

const mapStateToProps = (state: RootState): LoginComponentState => {
  return {
    token: state.Auth.token,
    username: state.Auth.username,
    loading: state.Auth.loading,
  };
};

const connectedLogin = connect(mapStateToProps, mapActionsToProps)(Login);
export default connectedLogin;
