import React from 'react';
import { Button } from 'rsuite';
import {LoginCredentials} from "../interfaces/components";
import LocaleMessage from "../components/localeMessage";


const Home: React.FC = () => {

	const login = (credentials: LoginCredentials) => () => {
	};

	const x: LoginCredentials = {
		username: 'maybe',
		password: 'maybe',
		email: 'maybe'
	};

	return (
		<React.Fragment>
			<Button
				appearance={'primary'}
				onClick={login(x)}
			>
				<LocaleMessage messageId={'general.login'}/>
			</Button>
		</React.Fragment>
	);
};

export default Home;
