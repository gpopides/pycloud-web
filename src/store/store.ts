import rootReducer from "./rootReducer";
import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from "redux-saga";
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
import rootSaga from './rootSaga';

const history = createBrowserHistory();


const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer(history),
    applyMiddleware(
			routerMiddleware(history),
			sagaMiddleware
		)
);

sagaMiddleware.run(rootSaga);

export {store, history};
