import {RedirectActionInterface} from "../types/app";

enum AppConstants {
	REDIRECT_TO = "REDIRECT_TO"
}

const actions = {
	redirect: (url: string): RedirectActionInterface => {
		return {
			type: AppConstants.REDIRECT_TO,
			url
		}
	}
}

export { actions, AppConstants  }
