import {LoginRequestInterface} from '../types/auth';

enum actionConstants {
    LOGIN_REQUEST = 'LOGIN_REQUEST',
		LOGIN_REQUEST_SUCCESS = 'LOGIN_REQUEST_SUCCESS',
		LOGIN_REQUEST_ERROR = 'LOGIN_REQUEST_ERROR'
}

const actions = {
    loginRequest: (username: string | undefined, password: string | undefined): LoginRequestInterface => {
        return {
            type: actionConstants.LOGIN_REQUEST,
            username,
            password
        }
    },
};


export { actionConstants, actions }
