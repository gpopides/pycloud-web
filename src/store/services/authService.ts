import ApiClient from '../../apiClient.js';
import { LoginResponse } from "./interfaces/auth"

const loginService = (username: string, password: string): Promise<any> | LoginResponse => {
  return ApiClient.post('/auth/login/', {username, password})
    .then((response: any) => {
			return response.data
    })
    .catch((error: any) => {
			return {success: false}
    });
};

export {loginService};
