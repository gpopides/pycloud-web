export interface LoginResponse {
	success: boolean;
	token?: string;
	username?: string;
	user_id?: string;
}
