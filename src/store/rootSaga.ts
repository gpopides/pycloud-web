import { all } from 'redux-saga/effects';

import authSaga from './sagas/auth';
import appSaga from './sagas/app';

export default function* rootSaga () {
    yield all([
        authSaga(),
				appSaga()
    ])
}
