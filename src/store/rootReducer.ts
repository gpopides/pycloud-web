import authReducer from './reducers/auth';
import {combineReducers} from "redux";
import { connectRouter } from 'connected-react-router'



const rootReducer = (history: any) => combineReducers({
		router: connectRouter(history),
    Auth: authReducer
} as any);

export default rootReducer;
