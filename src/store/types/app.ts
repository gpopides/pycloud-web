export interface RedirectActionInterface {
	type: string;
	url: string;
}
