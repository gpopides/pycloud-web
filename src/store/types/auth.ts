export interface RootState {
    Auth: AuthState
}

export interface AuthState {
    token: string | null;
		user_id: number | null;
		username: string | null;
		loading: boolean;
}



export interface LoginRequestInterface {
    type: string | undefined;
    username: string | undefined;
    password: string | undefined;
}
