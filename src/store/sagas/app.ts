import {put, takeEvery, fork, all, call} from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {AppConstants} from '../actions/app';
import {RedirectActionInterface} from '../types/app';
import {convertMessage} from '../../utils/generalUtils';

function* redirect(action: RedirectActionInterface) {
  const {url} = action;
  yield put(push(url));
}

function* watchRedirect() {
  yield takeEvery(AppConstants.REDIRECT_TO, redirect);
}

export default function* authSaga() {
  yield all([fork(watchRedirect)]);
}
