import {put, takeEvery, fork, all, call} from 'redux-saga/effects';
import {Notification} from 'rsuite';
import {actionConstants} from '../actions/auth';
import {AppConstants} from '../actions/app';
import {LoginRequestInterface} from '../types/auth';
import {loginService} from '../services/authService';
import {LoginResponse} from '../services/interfaces/auth';
import {convertMessage} from '../../utils/generalUtils';

function* loginRequest(action: LoginRequestInterface) {
  const {username, password} = action;
  const responseData: LoginResponse = yield call(
    loginService as any,
    username,
    password,
  );

  const {success, ...payload} = responseData;

  if (responseData.success) {
    Notification.success({
      title: convertMessage('general.login.success'),
      description:
        convertMessage('general.login.welcome') + `${payload.username}`,
    });
    yield all([
      put({type: actionConstants.LOGIN_REQUEST_SUCCESS, payload}),
      put({type: AppConstants.REDIRECT_TO, url: 'home'}),
    ]);
  } else {
    Notification.error({
      title: convertMessage('general.error'),
      description:
        convertMessage('general.login.error')
    });
    yield put({type: actionConstants.LOGIN_REQUEST_ERROR, payload: {}});
  }
}

function* watchLoginRequest() {
  yield takeEvery(actionConstants.LOGIN_REQUEST, loginRequest);
}

export default function* authSaga() {
  yield all([fork(watchLoginRequest)]);
}
