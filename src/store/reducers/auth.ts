import {AuthState} from "../types/auth";
import {actionConstants} from "../actions/auth";

const authState: AuthState = {
    token: null,
		user_id: null,
		username: null,
		loading: false
};

const authReducer = (state = authState, action: any) => {
    switch (action.type) {
        case actionConstants.LOGIN_REQUEST:
					return {...state, loading: true}
				case actionConstants.LOGIN_REQUEST_SUCCESS:
				case actionConstants.LOGIN_REQUEST_ERROR:
					return {...state, ...action.payload, loading: false}
        default:
            return state;
    }
};


export default authReducer;
