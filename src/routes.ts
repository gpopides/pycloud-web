import React from "react";

import App from "./App"
import Register from "./containers/auth/register"


type RouteCollectionItem = {
	path: string;
	component: React.FC | React.Component;
}


const routes: RouteCollectionItem[] = [
	{
		path: "/",
		component: App
	},
	{
		path: "/register",
		component: Register
	}
];

export default routes
