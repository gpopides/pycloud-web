export interface LoginComponentState {
    token: string | null;
    username: string | null;
    loading: boolean;
}
