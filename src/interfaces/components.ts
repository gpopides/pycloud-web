type LocaleMessageId = { messageId: string  }

export interface LoginCredentials {
  username: string;
  email: string;
  password: string;
}

export interface LocaleMessageProps {
  messageId: string;
}

export interface PycloudNotificationProps {
  title: LocaleMessageId;
  description: LocaleMessageId | React.Component;
  type: any;
}
