import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import 'rsuite/dist/styles/rsuite-default.css';
import {store, history} from './store/store';
import {Route} from "react-router";
import { ConnectedRouter } from 'connected-react-router'
import routes from './routes';

ReactDOM.render(
    <Provider store={store}>
		<ConnectedRouter history={history}>
				{routes.map((route: any) => {
					return <Route path={route.path} component={route.component} exact />;
				})}
		</ConnectedRouter>
	</Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
