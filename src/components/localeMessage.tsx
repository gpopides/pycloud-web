import React from 'react';
import {LocaleMessageProps} from '../interfaces/components';
import {convertMessage} from "../utils/generalUtils"


const LocaleMessage: React.FC<LocaleMessageProps> = (props: LocaleMessageProps) => {

	return (
		<React.Fragment>
			{convertMessage(props.messageId)}
		</React.Fragment>
	)
};

export default LocaleMessage;
